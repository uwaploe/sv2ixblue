// Sv2ixblue sends sound velocity sensor data to an iXBlue Rovins INS
package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime"
	"syscall"

	svs "bitbucket.org/uwaploe/go-svs"
	"bitbucket.org/uwaploe/ixblue"
	stan "github.com/nats-io/stan.go"
	"github.com/vmihailenco/msgpack"
)

const Usage = `Usage: sv2ixblue [options] host:port

Subscribe to the sound velocity data and generate ixBlue external sensor messages to
send to the Rovins INS at HOST:PORT.
`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	debugMode = flag.Bool("debug", false,
		"Log debugging output")
	natsURL   string = "nats://localhost:4222"
	clusterID string = "must-cluster"
	svSubject string = "sv.data"
	protocol  string = "tcp"
)

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&natsURL, "nats-url", lookupEnvOrString("NATS_URL", natsURL),
		"URL for NATS Streaming Server")
	flag.StringVar(&clusterID, "cid", lookupEnvOrString("NATS_CLUSTER_ID", clusterID),
		"NATS cluster ID")
	flag.StringVar(&svSubject, "sub", lookupEnvOrString("SV_SUBJECT", svSubject),
		"Subject name for INS data")
	flag.StringVar(&protocol, "proto", protocol, "Network protocol for Rovins input")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func main() {
	args := parseCmdLine()
	if len(args) == 0 {
		flag.Usage()
		os.Exit(1)
	}

	rovinsAddr := args[0]

	var status int
	defer func() { os.Exit(status) }()

	sc, err := stan.Connect(clusterID, "rovins-sv", stan.NatsURL(natsURL))
	if err != nil {
		log.Printf("Cannot connect: %v", err)
		status = 1
		return
	}
	defer sc.Close()

	conn, err := net.Dial(protocol, rovinsAddr)
	if err != nil {
		log.Print("Cannot access Rovins")
		status = 1
		return
	}

	svCb := func(m *stan.Msg) {
		var rec svs.Sample
		err := msgpack.Unmarshal(m.Data, &rec)
		if err != nil {
			log.Printf("Msgpack decode error: %v", err)
			return
		}
		enc := ixblue.NewEncoder(conn)
		block := ixblue.SoundVel{
			Tvalid: ixblue.Ticks(rec.T.UTC()),
			Svel:   rec.Sv,
		}
		if err := enc.Encode(&block); err != nil {
			log.Printf("Error sending SV data: %v", err)
		}
	}

	sub, err := sc.Subscribe(svSubject, svCb)
	if err != nil {
		log.Fatalf("Cannot subscribe to SV data: %v", err)
	}
	defer sub.Unsubscribe()

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	log.Printf("Rovins SV service starting %s", Version)

	// Exit on a signal
	<-sigs
}
