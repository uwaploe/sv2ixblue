module bitbucket.org/uwploe/sv2ixblue

go 1.15

require (
	bitbucket.org/uwaploe/go-svs v0.2.1
	bitbucket.org/uwaploe/ixblue v0.2.2
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/nats-io/nats-server/v2 v2.2.0 // indirect
	github.com/nats-io/nats-streaming-server v0.21.1 // indirect
	github.com/nats-io/stan.go v0.8.3
	github.com/vmihailenco/msgpack v4.0.4+incompatible
	google.golang.org/appengine v1.6.7 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
